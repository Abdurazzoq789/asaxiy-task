<p align="center">
    <h1 align="center">Asaxiy task </h1>
    <br>
</p>


CONFIGURATION
-------------

### Database

.env.example filni .env ga copy qilinadi so'ng kerakli configuratsialar kiritiladi

```php
# Framework
# ---------
YII_DEBUG   = true
YII_ENV     = dev

# Databases
# ---------
DB_DSN           = pgsql:host=localhost;dbname=asaxiy_task
DB_USERNAME      = 
DB_PASSWORD      = 
DB_CHARSET       = utf8
DB_TABLE_PREFIX  =
```
Php initialize qilindai dev mod tanlanadi
~~~
php init
~~~

* composer
~~~
composer install 
~~~
* Note Agar composer install bo'masa shu buyuq yurgaziladi
~~~
composer install --ignore-platform-reqs
~~~

* Migratsialar yurgaziladi
~~~
php yii migrate
~~~

* Applicationlar ishga tushiriladi agar web server bo'lsa web server orqali ishga tushiriladi https://www.yiiframework.com/wiki/799/yii2-app-advanced-on-single-domain-apache-nginx
* Yoki quydagi buruqlar alohida terminallarda amalga oshiriladi
~~~
./yii serve -p 8050 -t frontend/web 
~~~
Fronted http://localhost:8050
~~~
./yii serve -p 8051 -t backend/web 
~~~
Backend http://localhost:8051
~~~
./yii serve -p 8052 -t api/web 
~~~
Api http://localhost:8052

* Adminkaga kirish uchun login parol
~~~
Login : admin
Parol : admin123
~~~


### Api Endpoints

* Api endpointlarni  quydagi link orqali import qilib olish mumkin https://www.getpostman.com/collections/57776e464c6faa39486d

1. Candidatlar listini olish  `GET` `http://localhost:8052/v1/candidate`
2. Bitta Candidateni ko'rish `GET` `http://localhost:8052/v1/candidate/<id>`
3. Ariza Topshirish `POST`  `http://localhost:8052/v1/candidate/send-request`

```js
    {
        "name": "Abdurahim", 
        "family_name" : "Qurdatov",
        "address" : "Tashkent olmazor",
        "country_of_origin" : "Country",
        "email_address" : "abdurahim@gmail.com",
        "phone_number" : "+998974041602",
        "age" : 28
    }
```

4. Interview belgilash `POST` `http://localhost:8052/v1/candidate/<candidate_id>/mark-interview`
```js
    {
        "note" : "bir oydan  keyin ko'rishamiz",
        "interview_date": 1664514370
    }
```

5. Candidate update `PUT` `http://localhost:8052/v1/candidate/<id>`
```js
    {
        "name": "Abdurahim", 
        "family_name" : "Qurdatov",
        "address" : "Tashkent olmazor",
        "country_of_origin" : "Country",
        "email_address" : "abdurahim@gmail.com",
        "phone_number" : "+998974041602",
        "age" : 28
    }
```
6. Condidateni o'chrish `DELETE` `http://localhost:8052/v1/candidate/<id>`

* Candidat uchun constatalar
    `STATUS`
```
    const STATUS_NEW = 1;

    const STATUS_SCHEDULED = 10;

    const STATUS_ACCEPTED = 20;

    const STATUS_NOT_ACCEPTED = 25;

    const HIRED = true;

    const UNKNOWN_HIRED = false;
```