<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%candidate}}`.
 */
class m220830_084018_create_candidate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%candidate}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'family_name' => $this->string(),
            'address' => $this->string(),
            'country_of_origin' => $this->string(),
            'email_address' => $this->string(),
            'phone_number' => $this->string(),
            'age' => $this->integer(),
            'hired' => $this->boolean(),
            'status' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%candidate}}');
    }
}
