<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%candidate}}`.
 */
class m220831_083213_add_interview_date_column_to_candidate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%candidate}}', 'interview_date', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%candidate}}', 'interview_date');
    }
}
