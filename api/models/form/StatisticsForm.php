<?php

namespace api\models\form;

use common\modules\customer\models\Customer;
use yii\data\ActiveDataProvider;
use yii\db\Query;

class StatisticsForm extends \yii\base\Model
{
	public $from;
	public $to;

	public function byGender()
	{
		$query = (new Query())
			->from('customer')
			->leftJoin('call_history h', 'customer.id = h.customer_id')
			->andWhere(['between', 'h.created_at', $this->from, $this->to])
			->select('gender, count(gender)')
			->groupBy('gender');

		return new ActiveDataProvider(['query' => $query]);
	}

	public function byAge()
	{
		$subquery1 = (new Query())
			->select(['count(ch.id) as "14-18"'])
			->from("customer")
			->leftJoin("call_history ch", "customer.id = ch.customer_id")
			->andWhere(['between', "(date_part('year', CURRENT_DATE) - date_part('year', to_timestamp(birthdate)))", 14, 18])
			->andWhere(['between', 'ch.created_at', $this->from, $this->to]);

		$subquery2 = (new Query())
			->select(['count(ch.id) as "19-25"'])
			->from("customer")
			->leftJoin("call_history ch", "customer.id = ch.customer_id")
			->andWhere(['between', "(date_part('year', CURRENT_DATE) - date_part('year', to_timestamp(birthdate)))", 19, 25])
			->andWhere(['between', 'ch.created_at', $this->from, $this->to]);

		$subquery3 = (new Query())
			->select(['count(ch.id) as "26-35"'])
			->from("customer")
			->leftJoin("call_history ch", "customer.id = ch.customer_id")
			->andWhere(['between', "(date_part('year', CURRENT_DATE) - date_part('year', to_timestamp(birthdate)))", 26, 35])
			->andWhere(['between', 'ch.created_at', $this->from, $this->to]);

		$subquery4 = (new Query())
			->select(['count(ch.id) as "36-60"'])
			->from("customer")
			->leftJoin("call_history ch", "customer.id = ch.customer_id")
			->andWhere(['between', "(date_part('year', CURRENT_DATE) - date_part('year', to_timestamp(birthdate)))", 36, 60])
			->andWhere(['between', 'ch.created_at', $this->from, $this->to]);

		$query = (new Query())->select([$subquery1, $subquery2, $subquery3, $subquery4]);

		return $query->one();
	}

	public function byRegion()
	{
		$lang = \Yii::$app->language;

		$subQuery = (new Query())->from("call_history ch")
			->select("count(ch.id)")
			->leftJoin("customer c", "c.id = ch.customer_id")
			->andWhere('c.region_id = r.id')
			->andWhere(['between', 'ch.created_at', $this->from, $this->to]);

		$query = (new Query())
			->from('region r')
			->select(["(r.name->>$$$lang$$) name", $subQuery])
			->groupBy('r.id');

		return new ActiveDataProvider(['query' => $query]);
	}

	public function byService()
	{
		$lang = \Yii::$app->language;

		$subQuery = (new Query())->from("call_history ch")
			->select("count(ch.id)")
			->leftJoin("call_history_service chs", "chs.call_history_id = ch.id")
			->andWhere('chs.service_id = s.id')
			->andWhere(['between', 'ch.created_at', $this->from, $this->to]);

		$query = (new Query())
			->from('service s')
			->select(["(s.name->>$$$lang$$) name", $subQuery])
			->groupBy('s.id');

		return new ActiveDataProvider(['query' => $query]);
	}

	public function bySource()
	{
		$lang = \Yii::$app->language;

		$subQuery = (new Query())->from("call_history ch")
			->select("count(ch.id)")
			->leftJoin("customer c", "c.id = ch.customer_id")
			->andWhere('c.source_id = s.id')
			->andWhere(['between', 'ch.created_at', $this->from, $this->to]);

		$query = (new Query())
			->from('source s')
			->select(["(s.name->>$$$lang$$) name", $subQuery])
			->groupBy('s.id');

		return new ActiveDataProvider(['query' => $query]);
	}
}