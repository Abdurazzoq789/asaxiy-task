<?php


namespace api\models\form;


use common\modules\user\models\User;
use common\modules\user\models\UserEmailConfirmation;
use yii\base\BaseObject;

class RegisterForm extends \yii\base\Model
{

    public $email;


    public $password;

    public $first_name;

    public $last_name;

    public $phone;

    public $birthday;

    public $gender;

    private $_confirmation;

    public function formName()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password', 'first_name', 'last_name', 'gender'], 'required'],
            ['gender', 'integer'],
            [['email'], 'email'],
            [['birthday'], 'integer'],
            [['first_name', 'last_name', 'phone'], 'string'],
            ['email', 'unique', 'targetClass' => User::class],
            ['phone', 'unique', 'targetClass' => User::class],
            [['password'], 'string', 'min' => 6],
        ];
    }


    public function save()
    {

        if (!$this->validate()) {
            return false;
        }

        $user = $this->createUser();

        if ($this->_confirmation !== null) {
            /**
             * @var UserEmailConfirmation $model
             */
            $model = $this->_confirmation;
            $model->setResponseCode(101);
            $model->setResponseBody(true);

            return $model;
        }
        return $user;
    }

    /**
     * @return User|false
     * @throws \yii\base\Exception
     */
    private function createUser()
    {

        $user = new User();
        $user->setAttributes([
            'email' => $this->email,
            'status' => User::STATUS_INACTIVE,
            'role' => User::ROLE_USER,
            'username' => $this->email,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone' => $this->phone,
            'birthday' => $this->birthday,
            'gender' => $this->gender,
        ]);


        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->setToken();
//        $user->generateEmailVerificationToken();
        if ($user->save()) {
            $this->_confirmation = $user->sendEmailConfirmationCode();
            return $user;
        }
        return $user;
    }
}
