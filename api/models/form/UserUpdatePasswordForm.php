<?php

namespace api\models\form;

use common\modules\user\models\User;
use Yii;
use yii\base\Model;

/**
 * Update form
 */
class UserUpdatePasswordForm extends Model
{
	public $old_password;
	public $password;
	public $password_confirm;
	/**
	 * @var
	 */
	private $_user;

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['old_password', 'password', 'password_confirm'], 'trim'],
			[['old_password', 'password', 'password_confirm'], 'required'],
			['old_password', 'validatePassword'],
			[['password'], 'string', 'min' => 4],
			[['password'], 'compare', 'compareAttribute' => 'password_confirm'],
		];
	}

	public function validatePassword($attribute, $params)
	{
		if (!$this->hasErrors()) {
			$user = $this->getUser();
			if (!$user || !$user->validatePassword($this->old_password)) {
				$this->addError($attribute, "Old password incorrect");
			}
		}
	}

	public function save()
	{
		if (!$this->validate()) {
			return false;
		}

		/**
		 * @var $user User
		 */

		$user = $this->getUser();

		$user->setPassword($this->password);

		$user->save();

		return $user;
	}

	/**
	 * Finds user by [[username]]
	 *
	 * @return User|null
	 */
	protected function getUser()
	{
		if ($this->_user === null) {
			$this->_user = User::findIdentity(Yii::$app->user->id);
		}

		return $this->_user;
	}
}
