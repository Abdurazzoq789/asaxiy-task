<?php

namespace api\models\form;

use common\modules\user\models\User;
use Yii;
use yii\base\Model;

/**
 * Update form
 */
class UserUpdateForm extends Model
{
	public $username;

	/**
	 * @var
	 */
	private $_user;

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['username'], 'trim'],
			[['username'], 'required'],
			[['username'], 'string', 'min' => 3],
			[['username'], 'unique', 'targetClass' => User::class, 'targetAttribute' => ['username' => 'username']],
		];
	}

	/**
	 * @return array|bool|User|null
	 */
	public function save()
	{
		if (!$this->validate()) {
			return false;
		}

		/**
		 * @var $user User
		 */

		$user = $this->getUser();

		$user->updateAttributes([
			'username' => $this->username
		]);

		return $user;
	}

	/**
	 * Finds user by [[username]]
	 *
	 * @return User|null
	 */
	protected function getUser()
	{
		if ($this->_user === null) {
			$this->_user = User::findIdentity(Yii::$app->user->id);
		}

		return $this->_user;
	}
}
