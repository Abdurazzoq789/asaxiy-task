<?php

namespace api\models\form;

use common\modules\call_history\models\CallHistory;
use common\modules\call_history_service\models\CallHistoryService;
use common\modules\customer\models\Customer;

class CallHistoryForm extends \yii\base\Model
{
	public $comment;
	public $name;
	public $phone;
	public $customer_id;
	public $call_history_id;
	public $gender;
	public $birthdate;
	public $source_id;
	public $region_id;
	public $district_id;
	public $service_ids;

	public function rules()
	{
		return [
			[['comment', 'name', 'phone'], 'string'],
			[['customer_id', 'gender', 'birthdate', 'source_id', 'region_id', 'district_id', 'call_history_id'], 'integer'],
			[['service_ids'], 'safe'],
		];
	}

	public function formName()
	{
		return '';
	}

	public function save()
	{
		if (!$this->validate()) {
			return false;
		}

		if (!$this->customer_id) {
			$customer = new Customer([
				'name' => $this->name,
				'phone' => $this->phone,
				'gender' => $this->gender,
				'birthdate' => $this->birthdate,
				'source_id' => $this->source_id,
				'region_id' => $this->region_id,
				'district_id' => $this->district_id,
			]);

			if (!$customer->save()) {
				return $customer;
			}

			$this->customer_id = $customer->id;
		} else {
			$customer = Customer::findOne($this->customer_id);

			$customer->updateAttributes([
				'name' => $this->name,
				'phone' => $this->phone,
				'gender' => $this->gender,
				'birthdate' => $this->birthdate,
				'source_id' => $this->source_id,
				'region_id' => $this->region_id,
				'district_id' => $this->district_id,
			]);
		}
		if ($this->call_history_id) {
			$call_history = $this->updateCallHistory();
		} else {
			$call_history = $this->createCallHistory();
		}

		$services = $this->createCallHistoryServices();

		$response = [
			'customer' => $customer->attributes,
			'call_history' => $call_history->attributes,
			'call_history_services' => $services,
		];

		return $response;
	}

	public function updateCallHistory()
	{
		if ($model = CallHistory::findOne($this->call_history_id)) {
			if (!$model->customer_id){
				$model->updateAttributes([
					'customer_id' => $this->customer_id,
				]);
			}
			$model->updateAttributes([
				'comment' => $this->comment,
			]);
		}

		if (!$model->save()) {
			return $model;
		}

		return $model;
	}

	public function createCallHistory()
	{
		$model = new CallHistory([
			'comment' => $this->comment,
			'customer_id' => $this->customer_id,
            'status' => CallHistory::STATUS_SELF
		]);

		if (!$model->save()) {
			return $model;
		}
		$this->call_history_id = $model->id;

		return $model;
	}

	public function createCallHistoryServices()
	{
		$response = [];
		foreach ($this->service_ids as $service_id) {
			$model = new CallHistoryService([
				'service_id' => $service_id,
				'call_history_id' => $this->call_history_id,
			]);
			if ($model->save()) {
				$response[] = [$service_id => $model->attributes];
			} else {
				$response[] = [$service_id => $model->errors];
			}
		}

		return $response;
	}
}