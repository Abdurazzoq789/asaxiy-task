<?php

namespace api\modules\v1\controllers;

use common\components\ApiController;

/**
 * Default controller for the `v1` module
 */
class DefaultController extends ApiController
{
    public $serializer = [
        'class' => 'common\components\Serializer',
    ];
    public function actions()
    {
        return [

        ];
    }

    /**
     * Renders the index view for the module
     */
    public function actionIndex()
    {
        return array(
            "status" => "ok",
            "message" => "Welcome to Asaxiy Task Api v1"
        );
    }


}
