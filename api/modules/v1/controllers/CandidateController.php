<?php

namespace api\modules\v1\controllers;

use common\components\CrudController;
use common\models\Candidate;
use common\models\form\CandidateRequestForm;
use common\models\form\MarkInterviewForm;
use common\models\search\CandidateSearch;
use Yii;

class CandidateController extends CrudController
{
	public $modelClass = Candidate::class;
	public $searchModel = CandidateSearch::class;

    public function actionSendRequest()
    {
        $model = new CandidateRequestForm();
        $model->setAttributes(Yii::$app->request->post());
        if ($model->send()) {
            return [
                'code' => 1,
                'message' => 'Your request successfully send'
            ];
        }

        return $model;
    }

    public function actionMarkInterview($candidate_id)
    {
        $model = new MarkInterViewForm();
        $model->candidate_id = $candidate_id;
        $model->setAttributes(Yii::$app->request->post());

        if ($data = $model->save()){
            return $data;
        }

        return $model;
    }
}
