<?php

namespace api\modules\v1;

use yii\base\Module;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;

/**
 * v1 module definition class
 */
class ApiModule extends Module
{
	public static $urlRules = [
		[
			'class' => 'yii\rest\UrlRule',
			'controller' => 'v1',
			'pluralize' => false,
			'patterns' => [
				'GET' => 'default/index',
				'GET clear-cache' => 'default/clear-cache',
			]
		],
		[
			'class' => 'yii\rest\UrlRule',
			'controller' => 'v1/candidate',
			'pluralize' => false,
			'patterns' => [
				'OPTIONS <action>' => 'options',
				'OPTIONS' => 'options',

				'POST' => 'create',
				'GET' => 'index',

				'OPTIONS <id:\d+>' => 'options',
				'PUT <id:\d+>' => 'update',
				'GET <id:\d+>' => 'view',
				'DELETE <id:\d+>' => 'delete',

                'OPTIONS send-request' => 'options',
                'POST send-request' => 'send-request',

                'OPTIONS <candidate_id:\d+>/mark-interview' => 'options',
                'POST <candidate_id:\d+>/mark-interview' => 'mark-interview',
            ]
		],
	];

	/**
	 * {@inheritdoc}
	 */

	public $controllerNamespace = 'api\modules\v1\controllers';

	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();
	}

	public function behaviors()
	{
		$behaviors = parent::behaviors();

		unset($behaviors['authenticator']);


		$behaviors['authenticator'] = [
			'class' => CompositeAuth::class,
			'except' => [
				'default/*',
				'candidate/*',
			],
			'optional' => [],
			'authMethods' => [
				HttpBearerAuth::class,
			],
		];

		$behaviors['corsFilter'] = [
			'class' => Cors::class,
			'cors' => [
				'Origin' => static::allowedDomains(),
				'Access-Control-Request-Method' => ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'OPTIONS', 'DELETE'],
				'Access-Control-Max-Age' => 3600,
				'Access-Control-Request-Headers' => ['*'],
				'Access-Control-Expose-Headers' => ['*'],
				'Access-Control-Allow-Credentials' => false,
				'Access-Control-Allow-Methods' => ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'OPTIONS', 'DELETE'],
				'Access-Control-Allow-Headers' => ['Authorization', 'X-Requested-With', 'content-type'],
			],
		];

		return $behaviors;
	}

	/**
	 * @return array
	 */
	public static function allowedDomains()
	{
		return [
			'*',
		];
	}
}
