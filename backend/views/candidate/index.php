<?php

use common\models\Candidate;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CandidateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Candidates';
$this->params['breadcrumbs'][] = $this->title;
$formatter = Yii::$app->formatter;
$formatter->nullDisplay = '-';
?>
<div class="candidate-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Candidate', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'family_name',
            'email_address:email',
            'phone_number',
            'age',
            'hired:boolean',
            'note',
            'interview_date',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return Candidate::getStatusList()[$model->status];
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    Candidate::getStatusList(),
                    ['class' => 'form-control', 'prompt' => 'Все']
                )
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Candidate $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>
