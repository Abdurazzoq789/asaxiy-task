<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\FontAwesomeAsset;

/* @var $this yii\web\View */
/* @var $model common\models\Candidate */
/* @var $form yii\widgets\ActiveForm */

FontAwesomeAsset::register($this);
?>

<div class="candidate-form">
    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    <br>
                    <?= $form->field($model, 'family_name')->textInput(['maxlength' => true]) ?>
                    <br>
                    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                    <br>
                    <?= $form->field($model, 'country_of_origin')->textInput(['maxlength' => true]) ?>
                    <br>
                    <?= $form->field($model, 'age')->textInput() ?>
                    <br>
                    <?= $form->field($model, 'hired')->checkbox() ?>

                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <?= $form->field($model, 'email_address')->textInput(['maxlength' => true]) ?>
                    <br>
                    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
                    <br>
                    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>
                    <br>
                    <?= $form->field($model, 'interview_date')->widget(DateTimePicker::classname(), [
                        'options' => ['placeholder' => 'Enter event time ...'],
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ]) ?>
                    <br>
                    <?= $form->field($model, 'status')->dropDownList(\common\models\Candidate::getStatusList(), [
                            'prompt' => 'Select Status'
                    ]) ?>
                    <br>
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
