<?php

namespace common\models\form;

use common\models\Candidate;
use yii\base\Model;

class CandidateRequestForm extends Model
{
    public $name;

    public $family_name;

    public $address;

    public $country_of_origin;

    public $email_address;

    public $phone_number;
    public $hired;

    public $age;


    public function rules()
    {
        return [
            [['name', 'family_name', 'address', 'country_of_origin', 'email_address', 'phone_number', 'age'], 'required'],
            [['name', 'family_name'], 'string', 'min' => 5],
            [['address'], 'string', 'min' => 10],
            [['country_of_origin', 'email_address', 'phone_number'], 'string'],
            [['email_address'], 'email'],
            [['age'], 'integer'],
            [['hired'], 'boolean'],
            ['phone_number', 'phoneValidate'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @param $validator
     * @param $current
     */
    public function phoneValidate($attribute, $params, $validator, $current)
    {
        if (!$this->hasErrors()) {
            $current = preg_replace('/\s+/', '', $current);
            $regexPhoneEmail = '/(^(?:\d{9}|\+998\d{9})$)/';
            if (!preg_match($regexPhoneEmail, $current)) {
                $this->addError($attribute, 'Its not a phone number');
                return;
            }
            return;
        }
    }

    public function send()
    {
        if (!$this->validate()) {
            return false;
        }

        $model = new Candidate();

        $model->setAttributes($this->attributes);

        if ($model->save()){
            return true;
        }

        return  $model;
    }

}