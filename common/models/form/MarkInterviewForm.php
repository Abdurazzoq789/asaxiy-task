<?php

namespace common\models\form;

use common\models\Candidate;
use yii\base\Model;
use yii\web\NotFoundHttpException;

class MarkInterviewForm extends Model
{

    public $note;

    public $interview_date;

    public $candidate_id;

    public function rules()
    {
        return [
            [['note', 'interview_date', 'candidate_id'], 'required'],
            [['note'], 'string'],
            [['interview_date', 'candidate_id'], 'integer']
        ];
    }

    public function save()
    {
        if (!$this->validate()){
            return false;
        }

        $candidate = Candidate::findOne($this->candidate_id);
        if (!$candidate){
            throw new NotFoundHttpException("Candidate not found");
        }

        if ($candidate->status != Candidate::STATUS_NEW){
            $this->addError("status", "This candidate already marked");
            return false;
        }

        $candidate->setAttributes([
            'note' => $this->note,
            'interview_date' => $this->interview_date,
            'status' => Candidate::STATUS_SCHEDULED,
        ]);

        $candidate->save();

        return  $candidate;
    }

}